# frozen_string_literal: true

desc 'Download a fresh copy of libertarian-party.ru'
task :download do
  sh 'wget -c --mirror libertarian-party.ru'
end

desc 'Fix filenames, add .html extension'
task :fix_filenames do
  # Add .html to files without extension.
  FileList['libertarian-party.ru/**/*']
    .reject { |f| f.start_with?('libertarian-party.ru/system/images/') }
    .reject { |f| File.directory?(f) || File.basename(f).match?(/\.\D/) }
    .each { |f| File.rename(f, "#{f}.html") }

  # Substitute question marks.
  FileList['libertarian-party.ru/**/*\?*'].each do |f|
    if File.basename(f).include?('page')
      File.rename(f, f.sub('?', '_'))
    else
      File.rename(f, f.sub(/\?.*/, ''))
    end
  end

  # Remove numeric suffixes that wget produces.
  FileList['libertarian-party.ru/**/*.?.html'].each do |f|
    File.rename(f, f.sub(/\.\d\.html$/, '.html'))
  end
end

# I've tried various flags, but wget just fails to convert the links for some
# reason, so we're doing it here.
desc 'Make all links relative'
task :fix_links do
  FileList['libertarian-party.ru/**/*.{html,css}'].each do |page|
    content = File.read(page)
    content.gsub!(/https?:\/\/libertarian-?party.ru/, '')
    content.gsub!(/\?\d+/, '')
    File.write(page, content)
  end
end

desc 'Replace Google analytics and Yandex.Metrika with Matomo'
task :replace_scripts do
  matomo_js = File.read('matomo.js')
  matomo = <<~HTML
    <script type="text/javascript">#{matomo_js}</script>
    <noscript>
      <p>
        <img src="//matomo.libertarian-party.ru/piwik.php?idsite=2&rec=1"
              style="border:0;" alt="" />
      </p>
    </noscript>
  HTML

  FileList['libertarian-party.ru/**/*.html'].each do |page|
    content = File.read(page)
    content.sub!(/<!-- asynchronous google.*'script'\)\)<\/script>/m, matomo)
    content.sub!(/<!-- Yandex.*<!-- \/Yandex\.Metrika counter -->/m, '')
    File.write(page, content)
  end
end

desc 'Add a banner with a link to current website'
task :add_banner do
  banner = File.read('banner.html')
  FileList['libertarian-party.ru/**/*.html'].each do |page|
    File.write(page, File.read(page).sub('<body>', "<body>\n#{banner}"))
  end

  cp 'logo.svg', 'libertarian-party.ru/logo.svg'
end

desc 'Overwrite previous archive with the newly made one'
task :overwrite do
  archive_domain = 'archive.libertarian-party.ru'
  rm_r archive_domain if File.exist?(archive_domain)
  mv 'libertarian-party.ru', archive_domain
end

task default: %i[
  download
  fix_filenames
  fix_links
  replace_scripts
  add_banner
  overwrite
]
